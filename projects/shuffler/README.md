<!-- Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>. -->

# Shuffler

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Code scaffolding

Run `ng generate component component-name --project shuffler` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project shuffler`.
> Note: Don't forget to add `--project shuffler` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build shuffler` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build shuffler`, go to the dist folder `cd dist/shuffler` and run `npm publish`.

## Running unit tests

Run `ng test shuffler` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
