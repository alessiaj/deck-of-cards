// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Generates a random integer between bounds.
 * Lower bound is inclusive.
 * Upper bound is exclusive.
 * @param  upper The upper bound. Default is 10.
 * @param lower The lower bound. Default is 0.
 * @return The generated random number
 */
export const random = (upper = 10, lower = 0): number =>
  Math.floor((Math.random() * (upper - lower)) + lower);
