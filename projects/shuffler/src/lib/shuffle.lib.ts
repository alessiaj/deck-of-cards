// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { random } from './random.lib';

/**
 * Shuffles an array using the Durstenfeld's Fisher-Yates' shuffle algorithm.
 * Original "unshuffled" array is left untouched.
 * @param unshuffledArray An {@link Array} of object of type T to shuffle.
 * @return An {@link Array} of object of type T in a shuffled order.
 */
export const shuffleArray = <T>(unshuffledArray: Array<T>): Array<T> => {
  const shuffledArray: Array<T> = [...unshuffledArray];

  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = random(i + 1); // since random() does not include upper bound.
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }

  return shuffledArray;
};
