// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { random } from './random.lib';

describe('random', () => {
  it('should generate an integer >=0 and <10', () => {
    const result = random();
    expect(result)
      .toBeGreaterThanOrEqual(0, 'returned integer should greater than or equal to zero');
    expect(result)
      .toBeLessThan(10, 'returned integer should be less than 10');
  });

  it('should generate an integer >=0 and <20', () => {
    const result = random(20);
    expect(result)
      .toBeGreaterThanOrEqual(0, 'returned integer should be greated than or equal to zero');
    expect(result)
      .toBeLessThan(20, 'returned integer should be less than 10');
  });

  it('should generate an integer >=10 and <20', () => {
    const result = random(20, 10);
    expect(result)
      .toBeGreaterThanOrEqual(10, 'returned integer should be greater than or equal to 10');
    expect(result)
      .toBeLessThan(20, 'return integer should be less than 20');
  });
});
