// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { shuffleArray } from './shuffle.lib';

interface TestObject {
  key: string;
  value: string;
}

describe('shuffleArray', () => {
  it('should shuffle an array of numbers', () => {
    const unshuffledArray: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const result: Array<number> = shuffleArray<number>(unshuffledArray);
    expect(result).not
      .toEqual(unshuffledArray, 'unshuffled array should be left untouched');
  });

  it('should shuffle an array of objects', () => {
    const unshuffledArray: Array<TestObject> = [
      {key: 'key1', value: 'value1'},
      {key: 'key2', value: 'value2'},
      {key: 'key3', value: 'value3'},
      {key: 'key4', value: 'value4'},
      {key: 'key5', value: 'value5'}
    ];
    const result: Array<TestObject> = shuffleArray<TestObject>(unshuffledArray);
    expect(result).not
      .toEqual(unshuffledArray, 'unshuffled array should be left untouched');
  });
});
