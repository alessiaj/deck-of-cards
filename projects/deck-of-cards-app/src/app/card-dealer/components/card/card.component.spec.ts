// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankEnum } from '../../constants/rank.enum';
import { SuitEnum } from '../../constants/suit.enum';
import { CardComponent } from './card.component';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  describe(':', () => {
    it('should create', () => {
      expect(component)
        .toBeTruthy();
    });

    it('should hide the card by default', () => {
      expect(component.isShown)
        .toBeFalse('card is hidden by default');
    });
  });

  describe('::getFilename', () => {
    it("should return 'JOKER.png' if card is undefined", () => {
      expect(component.getFilename())
        .toEqual('JOKER.png', "'JOKER.png' default image for an unknown card");
    });

    it("should return 'BF.png' if card is hidden", () => {
      component.card = {
        id: 'queen_of_hearts',
        suit: SuitEnum.HEART,
        rank: RankEnum.QUEEN
      };
      expect(component.getFilename())
        .toEqual('BF.png', "'BF.png' is the image to show for a hidden card");
    });

    it("should return 'H12.png' if the card to show is the 'Queen of Hearts'", () => {
      component.isShown = true;
      component.card = {
        id: 'queen_of_hearts',
        suit: SuitEnum.HEART,
        rank: RankEnum.QUEEN
      };

      expect(component.getFilename())
        .toEqual('H12.png', "'H12.png' is the image for the 'Queen of Hearts'");
    });
  });

});
