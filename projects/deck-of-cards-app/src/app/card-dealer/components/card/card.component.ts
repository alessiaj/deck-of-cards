// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Component, Input } from '@angular/core';
import { Card } from '../../models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input() card: Card;
  @Input() isShown: boolean;

  private readonly BACKFACE: string;
  private readonly EXTENSION: string;
  private readonly JOKER: string;

  constructor() {
    this.BACKFACE = 'BF';
    this.JOKER = 'JOKER';
    this.EXTENSION = '.png';
    this.isShown = false;
  }

  getFilename(): string {
    if (this.card) {
      const filename: string = !this.isShown ? this.BACKFACE : `${this.card.suit}${this.card.rank}`;

      return `${filename}${this.EXTENSION}`;
    } else {
      return `${this.JOKER}${this.EXTENSION}`;
    }
  }
}
