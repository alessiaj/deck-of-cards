// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Deck } from '../../models/card';

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss']
})
export class CardContainerComponent implements OnChanges {

  @Input() deck: Deck;
  @Input() showDeck: boolean;

  mappedDeck: Map<number, Deck>;

  constructor() {
    this.showDeck = false;
    this.mappedDeck = new Map<number, Deck>();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.deck) {
      if (this.deck && this.deck.length) {
        this.mappedDeck.set(1, this.deck.slice(0, 13));
        this.mappedDeck.set(2, this.deck.slice(13, 26));
        this.mappedDeck.set(3, this.deck.slice(26, 39));
        this.mappedDeck.set(4, this.deck.slice(39));
      } else {
        this.mappedDeck.clear();
      }
    }
  }

}
