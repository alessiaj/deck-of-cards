// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankEnum } from '../../constants/rank.enum';
import { SuitEnum } from '../../constants/suit.enum';
import { Deck } from '../../models/card';
import { CardComponent } from '../card/card.component';
import { CardContainerComponent } from './card-container.component';

describe('CardContainerComponent', () => {
  let component: CardContainerComponent;
  let fixture: ComponentFixture<CardContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardContainerComponent, CardComponent]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  describe(':', () => {
    it('should create', () => {
      expect(component)
        .toBeTruthy();
    });

    it('should not show the deck by default', () => {
      expect(component.showDeck)
        .toBeFalse('cards are not shown by default');
    });

    it('should have an empty mapped deck by default', () => {
      expect(component.mappedDeck.size)
        .toEqual(0, 'the mapped deck if empty on creation');
    });
  });

  // tslint:disable:no-life-cycle-call
  describe('::ngOnChanges', () => {
    it('should do nothing if there is no change on the deck', () => {
      component.showDeck = true;
      fixture.detectChanges();
      expect()
        .nothing();
    });

    it('should clear the mapped deck if an empty deck is injected', () => {
      component.mappedDeck = new Map<number, Deck>([[1, []], [2, []]]);
      component.deck = [];
      component.ngOnChanges({deck: new SimpleChange(undefined, [], true)});
      expect(component.mappedDeck.size)
        .toEqual(0, 'the mapped deck should be cleared');
    });

    it('should fill the mapped deck when a deck is injected', () => {
      component.deck = [{id: 'queen_of_hearts', suit: SuitEnum.HEART, rank: RankEnum.QUEEN}];
      component.ngOnChanges({deck: new SimpleChange(undefined, [], true)});
      expect(component.mappedDeck.size)
        .toEqual(4, 'the mapped deck should be cleared');
    });
  });
  // tslint:enable:no-life-cycle-call
});
