// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Component } from '@angular/core';
import { shuffleArray } from 'shuffler';

import { createDeck } from '../../factories/deck.factory';
import { Card, Deck } from '../../models/card';
import { CardDealerService } from '../../services/card-dealer.service';

@Component({
  selector: 'app-card-dealer',
  templateUrl: './card-dealer.component.html',
  styleUrls: ['./card-dealer.component.scss']
})
export class CardDealerComponent {

  cardsToDeal: Deck;
  dealtCards: Deck;
  areCardsToDealShown: boolean;

  constructor(private cardDealerService: CardDealerService) {
    this.resetDeck();
    this.areCardsToDealShown = false;
  }

  showHideCardsToDeal(): void {
    this.areCardsToDealShown = !this.areCardsToDealShown;
  }

  getShowOrHide(): string {
    return this.areCardsToDealShown ? 'Hide' : 'Show';
  }

  resetDeck(): void {
    this.cardsToDeal = createDeck();
    this.dealtCards = [];
  }

  shuffleDeck(): void {
    this.cardsToDeal = shuffleArray<Card>(this.cardsToDeal);
  }

  dealCard(): void {
    const dealtCard: Card = this.cardDealerService.dealOneCard(this.cardsToDeal);
    if (dealtCard) {
      this.cardsToDeal = this.cardsToDeal.filter(card => card.id !== dealtCard.id);
      this.dealtCards = [...this.dealtCards, dealtCard];
    }
  }

}
