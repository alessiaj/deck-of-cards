// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankEnum } from '../../constants/rank.enum';
import { SuitEnum } from '../../constants/suit.enum';
import { Card, Deck } from '../../models/card';
import { CardDealerService } from '../../services/card-dealer.service';
import { CardContainerComponent } from '../card-container/card-container.component';
import { CardComponent } from '../card/card.component';
import { CardDealerComponent } from './card-dealer.component';

describe('CardDealerComponent', () => {
  let component: CardDealerComponent;
  let fixture: ComponentFixture<CardDealerComponent>;

  const cardDealerServiceSpy = jasmine.createSpyObj<CardDealerService>('CardDealerService', ['dealOneCard']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardDealerComponent, CardContainerComponent, CardComponent],
      providers: [
        {provide: CardDealerService, useValue: cardDealerServiceSpy}
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardDealerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  describe(':', () => {
    it('should create', () => {
      expect(component)
        .toBeTruthy();
    });

    it('should hide the cards to deal by default', () => {
      expect(component.areCardsToDealShown)
        .toBeFalse('cards to deal are hidden by default');
    });

    it('should create a default deck to deal', () => {
      expect(component.cardsToDeal)
        .toBeArrayOfSize(52, 'a regular deck of cards has 52 cards');
    });

    it('should not have any dealt cards', () => {
      expect(component.dealtCards)
        .toBeArrayOfSize(0, 'no cards should be dealt by default');
    });
  });

  describe('::showHideCardsToDeal', () => {
    it('should show the cards to deal if they are hidden', () => {
      component.areCardsToDealShown = false;
      component.showHideCardsToDeal();
      expect(component.areCardsToDealShown)
        .toBeTrue('if cards are hidden, it should toggle them to show');
    });

    it('should hide the cards to deal if they are shown', () => {
      component.areCardsToDealShown = true;
      component.showHideCardsToDeal();
      expect(component.areCardsToDealShown)
        .toBeFalse('if cards are shown, it should toggle them to hide');
    });
  });

  describe('::getShowOrHide', () => {
    it("should return 'Show' if cards to deal are hidden", () => {
      component.areCardsToDealShown = false;
      expect(component.getShowOrHide())
        .toEqual('Show', "if cards are hidden, return 'Show'");
    });

    it("should return 'Hide' if cards to deal are shown", () => {
      component.areCardsToDealShown = true;
      expect(component.getShowOrHide())
        .toEqual('Hide', "if cards are shown, return 'Hide'");
    });
  });

  describe('::reset', () => {
    it('should assign a regular deck for the cards to deal', () => {
      component.resetDeck();
      expect(component.cardsToDeal)
        .toBeArrayOfSize(52, 'a regular playing card deck has 52 cards');
    });

    it('should assign an empty an empty array for the dealt cards', () => {
      component.resetDeck();
      expect(component.dealtCards)
        .toBeEmptyArray('there are no dealt cards when deck reset');
    });
  });

  describe('::shuffleDeck', () => {
    it('should shuffle the deck', () => {
      const cardsToDeal: Deck = [
        {id: 'queen_of_hearts', suit: SuitEnum.HEART, rank: RankEnum.QUEEN},
        {id: 'queen_of_spade', suit: SuitEnum.SPADE, rank: RankEnum.QUEEN},
        {id: 'queen_of_diamond', suit: SuitEnum.DIAMOND, rank: RankEnum.QUEEN},
        {id: 'queen_of_club', suit: SuitEnum.CLUB, rank: RankEnum.QUEEN}
      ];
      component.cardsToDeal = cardsToDeal;
      component.shuffleDeck();
      expect(component.cardsToDeal === cardsToDeal)
        .toBeFalse('the shuffle assign a new deck in a different order');
      // order cannot be tested since it is random. Only the reference is tested.
    });
  });

  describe('::dealCard', () => {
    it('should not change the cards to deal or dealt card if the dealt card is undefined', () => {
      cardDealerServiceSpy.dealOneCard.and.returnValue(undefined);
      component.dealCard();
      expect(component.cardsToDeal)
        .toBeArrayOfSize(52, 'cards to deal should not change');
      expect(component.dealtCards)
        .toBeEmptyArray('a card should not be added ot this array');
    });

    it('should remove the dealt card from the cards to deal', () => {
      const dealtCard: Card = {id: 'queen_of_hearts', suit: SuitEnum.HEART, rank: RankEnum.QUEEN};
      const cardsToDeal: Deck = [
        dealtCard,
        {id: 'queen_of_spade', suit: SuitEnum.SPADE, rank: RankEnum.QUEEN},
        {id: 'queen_of_diamond', suit: SuitEnum.DIAMOND, rank: RankEnum.QUEEN},
        {id: 'queen_of_club', suit: SuitEnum.CLUB, rank: RankEnum.QUEEN}
      ];
      cardDealerServiceSpy.dealOneCard.and.returnValue(dealtCard);
      component.cardsToDeal = cardsToDeal;
      component.dealCard();
      expect(component.cardsToDeal)
        .toBeArrayOfSize(3, 'the deal card is removed from the cards to deal');
      expect(component.cardsToDeal).not
        .toContain(dealtCard, 'the dealt card is removed from the cards to deal');
    });

    it('should add the dealt card from the dealt cards', () => {
      const dealtCard: Card = {id: 'queen_of_hearts', suit: SuitEnum.HEART, rank: RankEnum.QUEEN};
      const cardsToDeal: Deck = [
        dealtCard,
        {id: 'queen_of_spade', suit: SuitEnum.SPADE, rank: RankEnum.QUEEN},
        {id: 'queen_of_diamond', suit: SuitEnum.DIAMOND, rank: RankEnum.QUEEN},
        {id: 'queen_of_club', suit: SuitEnum.CLUB, rank: RankEnum.QUEEN}
      ];
      cardDealerServiceSpy.dealOneCard.and.returnValue(dealtCard);
      component.cardsToDeal = cardsToDeal;
      component.dealCard();
      expect(component.dealtCards)
        .toBeArrayOfSize(1, 'the dealt card is added to the dealt cards');
      expect(component.dealtCards)
        .toContain(dealtCard, 'the dealt card is added to the dealt cards');
    });
  });
});
