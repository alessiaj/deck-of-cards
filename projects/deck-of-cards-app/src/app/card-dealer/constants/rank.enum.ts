// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

export enum RankEnum {
  ACE = 1,
  TWO = 2,
  THREE = 3,
  FOUR = 4,
  FIVE = 5,
  SIX = 6,
  SEVEN = 7,
  EIGHT = 8,
  NINE = 9,
  TEN = 10,
  JACK = 11,
  QUEEN = 12,
  KING = 13
}

export const ranks = [
  RankEnum.ACE,
  RankEnum.TWO,
  RankEnum.THREE,
  RankEnum.FOUR,
  RankEnum.FIVE,
  RankEnum.SIX,
  RankEnum.SEVEN,
  RankEnum.EIGHT,
  RankEnum.NINE,
  RankEnum.TEN,
  RankEnum.JACK,
  RankEnum.QUEEN,
  RankEnum.KING
];
