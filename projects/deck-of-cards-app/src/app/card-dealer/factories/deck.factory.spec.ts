// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { RankEnum } from '../constants/rank.enum';
import { SuitEnum } from '../constants/suit.enum';
import { createDeck } from './deck.factory';

describe('createDeck', () => {
  it('should return a regular deck containing 52 cards', () => {
    const expectedDeck: Array<{ suit: SuitEnum, rank: RankEnum }> = [
      {suit: SuitEnum.SPADE, rank: RankEnum.ACE},
      {suit: SuitEnum.SPADE, rank: RankEnum.TWO},
      {suit: SuitEnum.SPADE, rank: RankEnum.THREE},
      {suit: SuitEnum.SPADE, rank: RankEnum.FOUR},
      {suit: SuitEnum.SPADE, rank: RankEnum.FIVE},
      {suit: SuitEnum.SPADE, rank: RankEnum.SIX},
      {suit: SuitEnum.SPADE, rank: RankEnum.SEVEN},
      {suit: SuitEnum.SPADE, rank: RankEnum.EIGHT},
      {suit: SuitEnum.SPADE, rank: RankEnum.NINE},
      {suit: SuitEnum.SPADE, rank: RankEnum.TEN},
      {suit: SuitEnum.SPADE, rank: RankEnum.JACK},
      {suit: SuitEnum.SPADE, rank: RankEnum.QUEEN},
      {suit: SuitEnum.SPADE, rank: RankEnum.KING},
      {suit: SuitEnum.HEART, rank: RankEnum.ACE},
      {suit: SuitEnum.HEART, rank: RankEnum.TWO},
      {suit: SuitEnum.HEART, rank: RankEnum.THREE},
      {suit: SuitEnum.HEART, rank: RankEnum.FOUR},
      {suit: SuitEnum.HEART, rank: RankEnum.FIVE},
      {suit: SuitEnum.HEART, rank: RankEnum.SIX},
      {suit: SuitEnum.HEART, rank: RankEnum.SEVEN},
      {suit: SuitEnum.HEART, rank: RankEnum.EIGHT},
      {suit: SuitEnum.HEART, rank: RankEnum.NINE},
      {suit: SuitEnum.HEART, rank: RankEnum.TEN},
      {suit: SuitEnum.HEART, rank: RankEnum.JACK},
      {suit: SuitEnum.HEART, rank: RankEnum.QUEEN},
      {suit: SuitEnum.HEART, rank: RankEnum.KING},
      {suit: SuitEnum.CLUB, rank: RankEnum.ACE},
      {suit: SuitEnum.CLUB, rank: RankEnum.TWO},
      {suit: SuitEnum.CLUB, rank: RankEnum.THREE},
      {suit: SuitEnum.CLUB, rank: RankEnum.FOUR},
      {suit: SuitEnum.CLUB, rank: RankEnum.FIVE},
      {suit: SuitEnum.CLUB, rank: RankEnum.SIX},
      {suit: SuitEnum.CLUB, rank: RankEnum.SEVEN},
      {suit: SuitEnum.CLUB, rank: RankEnum.EIGHT},
      {suit: SuitEnum.CLUB, rank: RankEnum.NINE},
      {suit: SuitEnum.CLUB, rank: RankEnum.TEN},
      {suit: SuitEnum.CLUB, rank: RankEnum.JACK},
      {suit: SuitEnum.CLUB, rank: RankEnum.QUEEN},
      {suit: SuitEnum.CLUB, rank: RankEnum.KING},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.ACE},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.TWO},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.THREE},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.FOUR},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.FIVE},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.SIX},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.SEVEN},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.EIGHT},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.NINE},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.TEN},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.JACK},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.QUEEN},
      {suit: SuitEnum.DIAMOND, rank: RankEnum.KING}
    ];

    // IDs are removed since they are auto generated.
    const actualDeckWithoutIds: Array<{ suit: SuitEnum, rank: RankEnum }> = createDeck()
      .map(card => ({suit: card.suit, rank: card.rank}));

    expect(actualDeckWithoutIds.length)
      .toEqual(52, 'a standard playing card deck has 52 cards');
    expect(actualDeckWithoutIds)
      .toEqual(expectedDeck, 'actual deck should contain the same cards in the same order');
  });
});
