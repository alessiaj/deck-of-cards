// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { v4 } from 'uuid';

import { ranks } from '../constants/rank.enum';
import { suits } from '../constants/suit.enum';
import { Deck } from '../models/card';

export const createDeck = (): Deck => {
  return suits.reduce((deck, suit) => {
    deck.push(...ranks.map(rank => ({
      id: v4(),
      suit,
      rank
    })));

    return deck;
  }, []);
};
