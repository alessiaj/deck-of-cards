// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { inject, TestBed } from '@angular/core/testing';

import { RankEnum } from '../constants/rank.enum';
import { SuitEnum } from '../constants/suit.enum';
import { CardDealerService } from './card-dealer.service';

describe('CardDealerService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    providers: [CardDealerService]
  }));

  describe(':', () => {
    it('should be created', inject([CardDealerService], (service: CardDealerService) => {
      expect(service)
        .toBeTruthy();
    }));
  });

  describe('::dealOneCard', () => {
    it('should return undefined if the deck is undefined', inject([CardDealerService], (service: CardDealerService) => {
      expect(service.dealOneCard(undefined))
        .toBeUndefined('undefined is expected if deck is undefined');
    }));

    it('should return undefined if the deck is null', inject([CardDealerService], (service: CardDealerService) => {
      // tslint:disable-next-line:no-null-keyword
      expect(service.dealOneCard(null))
        .toBeUndefined('undefined is expected if deck is undefined');
    }));

    it('should return undefined if the deck is empty', inject([CardDealerService], (service: CardDealerService) => {
      // tslint:disable-next-line:no-null-keyword
      expect(service.dealOneCard([]))
        .toBeUndefined('undefined is expected if deck is empty');
    }));

    it('should return a card if the deck is defined and not empty', inject([CardDealerService], (service: CardDealerService) => {
      // tslint:disable-next-line:no-null-keyword
      expect(service.dealOneCard([{id: 'anId', suit: SuitEnum.HEART, rank: RankEnum.QUEEN}]))
        .toBeDefined('a card is expected if deck is defined and not empty');
    }));
  });
});
