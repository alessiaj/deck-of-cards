// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [
        RouterTestingModule,
        NgbCollapseModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(NavbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  describe(':', () => {
    it('should be collapsed on creation', () => {
      expect(component.isCollapsed)
        .toBe(true, 'navbar should be collapsed by default');
    });

    it('should have an empty list of navbar items on creation', () => {
      expect(component.navbarItems)
        .toBeEmptyArray('navbarItems should be empty by default');
    });

    it('should have an empty brand on creation', () => {
      expect(component.brand)
        .toBeEmptyString('brand should be an empty string by default');
    });
  });

  describe('::toggleCollapsed', () => {
    it('should invert the value of isCollapsed', () => {
      expect(component.isCollapsed)
        .toBeTrue('isCollapsed has a default value of true');
      component.toggleCollapsed();
      expect(component.isCollapsed)
        .toBeFalse('isCollapsed should have false value after call to toggleCollapsed');
    });
  });

  describe('::collapse', () => {
    it('should invert the value of isCollapsed', () => {
      component.isCollapsed = false;
      component.collapse();
      expect(component.isCollapsed)
        .toBeTrue('isCollapsed should have true value after call to collapse');
    });
  });
});
