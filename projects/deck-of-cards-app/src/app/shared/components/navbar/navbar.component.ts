// Copyright (C) 2019  Alessia Jauvin <alessia.jauvin@gmail.com>

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Component, Input } from '@angular/core';

import { NavbarItem } from '../../models/navbar-item';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  @Input() navbarItems: Array<NavbarItem>;
  @Input() brand: string;
  isCollapsed: boolean;

  constructor() {
    this.isCollapsed = true;
    this.navbarItems = [];
    this.brand = '';
  }

  toggleCollapsed = () => this.isCollapsed = !this.isCollapsed;

  collapse = () => this.isCollapsed = true;
}
